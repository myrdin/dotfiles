" File              : .vimrc
" Date              : 13.03.2018
" Last Modified Date: 13.03.2018
"General
syntax on	    "Set syntax highlighnting
set number	    "Show line numbers
set linebreak	    "Break lines at word (requires Wrap lines)
set showbreak=+++   "Wrap-broken line prefix
set textwidth=150   "Line wrap (number of cols)
set showmatch	    "Highlight matching brace
set visualbell	    "Use visual bell (no beeping)
 
set hlsearch	    "Highlight all search results
set smartcase	    "Enable smart-case search
set ignorecase	    "Always case-insensitive
set incsearch	    "Searches for strings incrementally
 
set autoindent	    "Auto-indent new lines
set shiftwidth=4    "Number of auto-indent spaces
set smartindent	    "Enable smart-indent
set smarttab	    "Enable smart-tabs
set softtabstop=4   "Number of spaces per Tab
set scrolloff=2	    "2 lines around the cursor

set ruler	    "how row and column ruler information

"Pathogen
execute pathogen#infect()

"Mapping
imap ii <Esc>
imap <C-@> <C-x><C-o> 

"No more directional keys
map <up> <nop>
map <down> <nop>
map <left> <nop>
map <right> <nop>
imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

"Netrw
let g:netrw_winsize = 22	"Netrw size
let g:netrw_browse_split = 3	"Open file in a new tab
let g:netrw_liststyle = 3	"List style for netrw
autocmd VimEnter * :Vexplore	"Open netrw at startup 
let g:netrw_banner = 0		"Remove the banner

"Front-end
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags	"Html autocompletion
autocmd FileType css set omnifunc=csscomplete#CompleteCSS	"Css autocompletion
autocmd BufNewFile *.html 0r ~/.vim/templates/skeleton.html	"Paste html template while opening *.html
